var numArr = [];
function nhapSo() {
  var numEl = document.getElementById("num");
  var numValue = numEl.value * 1;
  numArr.push(numValue);
  numEl.value = "";
  document.getElementById("result-1").innerText = numArr.join(" ; ");

  // số dương
  var positiveArr = [];
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      positiveArr.push(numArr[i]);
    }
  }

  var sumPositiveArr = 0;
  var minPositive = positiveArr[0];
  for (var i = 0; i < positiveArr.length; i++) {
    sumPositiveArr += positiveArr[i];
    var minPositiveCurrent = positiveArr[i];
    if (minPositiveCurrent < minPositive) {
      minPositive = minPositiveCurrent;
    }
  }

  document.getElementById("result-2").innerText = sumPositiveArr;
  document.getElementById("result-3").innerText = positiveArr.length;
  document.getElementById("result-5").innerText = minPositive;

  // số nhỏ nhất trong mảng
  var minNum = numArr[0];
  for (var i = 1; i < numArr.length; i++) {
    var minNumCurrent = numArr[i];
    if (minNumCurrent < minNum) {
      minNum = minNumCurrent;
    }
  }
  document.getElementById("result-4").innerText = minNum;

  // số chẵn trong mảng

  var soChanArr = [];
  var lastSochan = "";
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] % 2 === 0) {
      var soChan = numArr[i];
      soChanArr.push(soChan);
      lastSochan = soChanArr.pop();
      document.getElementById("result-6").innerText = lastSochan;
    }
  }
  var negativeArr = [];
  for (var i = 0; i < numArr.length; i++) {
    var num = numArr[i];
    if (num < 0) {
      negativeArr.push(num);
    }
  }

  
  
  // sắp xếp tăng dàn trong mảng
  var numArr2 = [...numArr]
   numArr2.sort(function (a, b) {
      return a - b;
    })
    document.getElementById("result-8").innerText = numArr2;



    // số nguyên tố
    var primeArr = [];
    for (var i = 0; i <= numArr.length; i++) {
    isPrime = true;
    for (var n = 2; n <= numArr[i]; n++) {
      if ((numArr[i] % n == 0 && n != numArr[i]) || numArr[i] == 0 || numArr[i] == 1) {
        isPrime = false;
      }
    }
    if(isPrime == true && numArr[i] !== 0 && numArr[i] !== 1 && numArr[i] !== undefined && numArr[i] > 0) {
      primeArr.push(numArr[i]);
      document.getElementById("result-9").innerText = primeArr[0]
    }
    
    }

    //so sánh số lượng số dương và số lượng số âm
  if (positiveArr.length > negativeArr.length) {
    document.getElementById("result-11").innerText =
    positiveArr.length +
      "số dương" +
      " " +
      " nhiều hơn " +
      negativeArr.length +
      "số âm";
  } else if (positiveArr.length < negativeArr.length) {
    document.getElementById("result-11").innerText =
    positiveArr.length +
      "số dương" +
      " " +
      " ít hơn" +
      negativeArr.length +
      "số âm";
  } else if (positiveArr.length == negativeArr.length) {
    document.getElementById("result-11").innerText =
    positiveArr.length +
      "số dương" +
      " " +
      " bằng số âm" +
      negativeArr.length +
      "số âm";
  }
}
/// đổi 2 vị trí trong mảng

function swapIndex() {
  var index1El = document.getElementById("index1");
  var index2El = document.getElementById("index2");
  var index1 = index1El.value * 1 - 1;
  var index2 = index2El.value * 1 - 1;

  const tmp = numArr[index1];
  numArr[index1] = numArr[index2];
  numArr[index2] = tmp;
  console.log(numArr);
  document.getElementById("swap-result-").innerText = numArr;
}
// 9.Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?

arrayNum = [];
document.getElementById('add-num').addEventListener('click', function() {
    var addNum = document.getElementById('so-nguyen');
    var addNumVal = addNum.value * 1;
    arrayNum.push(addNumVal);
    addNum.value= "";
    document.getElementById('result-12').innerHTML = `Kết quả : ${arrayNum}`;
});

document.getElementById('count-integers').addEventListener('click', function() {
    var check = 0;
    var total = 0;
    for(var i = 0; i < arrayNum.length; i++) {
        check = Number.isInteger(arrayNum[i]);
        if(check == true) {
            total ++;
        } 
    };
    document.getElementById('result-10').innerHTML = `Kết quả : ${total}`;
});

